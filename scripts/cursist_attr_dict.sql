set define '^'
set verify off
set serveroutput on size 1000000
set feedback off
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
 
prompt  Set Credentials...
 
begin
 
  -- Assumes you are running the script connected to sqlplus as the schema associated with the UI defaults or as the product schema.
  wwv_flow_api.set_security_group_id(p_security_group_id=>1700436502411350);
 
end;
/

begin wwv_flow.g_import_in_progress := true; end;
/
begin 

select value into wwv_flow_api.g_nls_numeric_chars from nls_session_parameters where parameter='NLS_NUMERIC_CHARACTERS';

end;

/
begin execute immediate 'alter session set nls_numeric_characters=''.,''';

end;

/
begin wwv_flow.g_browser_language := 'en'; end;
/
prompt  Check Compatibility...
 
begin
 
-- This date identifies the minimum version required to install this file.
wwv_flow_api.set_version(p_version_yyyy_mm_dd=>'2019.10.04');
 
end;
/

 
--------------------------------------------------------------------
prompt User Interface Defaults, Attribute Dictionary
--
-- Exported 18:24 Monday March 9, 2020 by: ADMIN_CURSIST
--
-- SHOW EXPORTING WORKSPACE
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_exp_workspace := 'CURSIST';
 
end;
/

 
begin
 
wwv_flow_hint.remove_col_attr_by_name('GROUP_ID');
wwv_flow_hint.create_col_attribute(
  p_label => 'Group',
  p_form_display_height => 1,
  p_report_col_alignment => 'LEFT',
  p_column_id => 3440231485903702 + wwv_flow_api.g_id_offset,
  p_column_name  => 'GROUP_ID');
wwv_flow_hint.create_col_synonym(
  p_syn_id => 3440357307903703 + wwv_flow_api.g_id_offset,
  p_column_id => 3440231485903702 + wwv_flow_api.g_id_offset,
  p_syn_name  => 'GROUP_ID');
 
end;
/

commit;
begin 
execute immediate 'alter session set nls_numeric_characters='''||wwv_flow_api.g_nls_numeric_chars||'''';
end;
/
set verify on
set feedback on
prompt  ...done
