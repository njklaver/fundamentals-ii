CREATE TABLE cursist.group_pages (
  "ID" NUMBER NOT NULL,
  "GROUP_ID" NUMBER,
  page_id NUMBER NOT NULL,
  CONSTRAINT group_pages_id_pk PRIMARY KEY ("ID"),
  CONSTRAINT group_pages_group_id_fk FOREIGN KEY ("GROUP_ID") REFERENCES cursist."GROUPS" ("ID") ON DELETE CASCADE,
  CONSTRAINT group_pages_page_id_fk FOREIGN KEY (page_id) REFERENCES cursist.pages ("ID")
);