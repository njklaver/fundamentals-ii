CREATE TABLE cursist.documents (
  "ID" NUMBER NOT NULL,
  "DOCUMENT" BLOB NOT NULL,
  date_created DATE NOT NULL,
  description VARCHAR2(2000 BYTE),
  filename VARCHAR2(255 BYTE),
  mimetype VARCHAR2(255 BYTE),
  last_update_date DATE,
  rating NUMBER,
  CONSTRAINT doc_pk PRIMARY KEY ("ID")
);