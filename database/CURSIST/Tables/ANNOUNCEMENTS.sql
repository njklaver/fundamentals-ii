CREATE TABLE cursist.announcements (
  "ID" NUMBER NOT NULL,
  user_id NUMBER,
  "GROUP_ID" NUMBER,
  indication_active VARCHAR2(1 BYTE) NOT NULL,
  announcement_text VARCHAR2(4000 BYTE),
  CONSTRAINT announcements_id_pk PRIMARY KEY ("ID"),
  CONSTRAINT announcements_group_id_fk FOREIGN KEY ("GROUP_ID") REFERENCES cursist."GROUPS" ("ID") ON DELETE CASCADE,
  CONSTRAINT announcements_user_id_fk FOREIGN KEY (user_id) REFERENCES cursist."USERS" ("ID") ON DELETE CASCADE
);