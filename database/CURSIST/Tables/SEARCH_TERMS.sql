CREATE TABLE cursist.search_terms (
  "ID" NUMBER(10) NOT NULL,
  date_searched DATE DEFAULT sysdate NOT NULL,
  term VARCHAR2(100 BYTE) NOT NULL,
  use_id NUMBER(10) NOT NULL,
  CONSTRAINT ste_pk PRIMARY KEY ("ID"),
  CONSTRAINT ste_use_fk FOREIGN KEY (use_id) REFERENCES cursist."USERS" ("ID")
);