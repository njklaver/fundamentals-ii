CREATE TABLE cursist.pages (
  "ID" NUMBER NOT NULL,
  apex_id NUMBER(*,0) NOT NULL,
  page_name VARCHAR2(255 BYTE) NOT NULL,
  page_title VARCHAR2(255 BYTE),
  CONSTRAINT pages_id_pk PRIMARY KEY ("ID")
);