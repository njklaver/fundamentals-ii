CREATE TABLE cursist.group_tabs (
  "ID" NUMBER(10) NOT NULL,
  gro_id NUMBER(10) NOT NULL,
  mta_id NUMBER(10) NOT NULL,
  CONSTRAINT gta_pk PRIMARY KEY ("ID"),
  CONSTRAINT gta_gro_fk FOREIGN KEY (gro_id) REFERENCES cursist."GROUPS" ("ID"),
  CONSTRAINT gta_mta_fk FOREIGN KEY (mta_id) REFERENCES cursist.main_tabs ("ID")
);