CREATE OR REPLACE PACKAGE cursist.back_office IS

  -- Author  : NICOJ
  -- Created : 16-11-2019 14:46:55
  -- Purpose : Back office Apex backend

  FUNCTION error_handling(p_error IN apex_error.t_error) RETURN apex_error.t_error_result;

END back_office;

/