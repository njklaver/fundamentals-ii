CREATE OR REPLACE PACKAGE cursist.user_security IS
/**
  * Password security API
  *
  * @author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Returns the Hash for the Username/Password supplied.
  *
  * @param in_username Username
  * @param in_password Password
  *
  * @return Hash for the Username/Password supplied. 
  */
  FUNCTION get_hash(in_username IN VARCHAR2,
                    in_password IN VARCHAR2) RETURN VARCHAR2;

END user_security;
/