CREATE OR REPLACE PACKAGE cursist.front_office IS
  -- Sub-Program Unit Declarations 
  FUNCTION check_user(p_username IN VARCHAR2,
                      p_password IN VARCHAR2) RETURN BOOLEAN;
  FUNCTION get_use_id(p_username IN VARCHAR2) RETURN NUMBER;
  FUNCTION page_valid(p_apex_id IN NUMBER,
                      p_usr_id  IN NUMBER) RETURN BOOLEAN;
  PROCEDURE post_authentication;
  FUNCTION get_highlight_text(p_index  IN VARCHAR2,
                              p_id     IN VARCHAR2,
                              p_query  IN VARCHAR2,
                              p_amount IN NUMBER := 100) RETURN VARCHAR2;
  PROCEDURE add_searchterm(in_searchterm IN VARCHAR2,
                           in_use_id     IN NUMBER);
END front_office;
/