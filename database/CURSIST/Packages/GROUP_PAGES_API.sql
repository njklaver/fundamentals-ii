CREATE OR REPLACE PACKAGE cursist.group_pages_api IS
  /**
  * Table API for GROUP_TABLES
  *
  * @author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Returns a : separated list of all pages for the group given. 
  *
  * @param in_group_id Group t handle.
  *
  * @return The  : separated list of all pages 
  */
  FUNCTION get(in_group_id IN NUMBER) RETURN VARCHAR2;

  /**
  * Save the selected pages in GROUP_PAGES
  *
  * @param in_group_id Group to handle
  * @param in_pag      : separated list of all authorized tables.
  */
  PROCEDURE put(in_group_id IN NUMBER,
                in_pag      IN VARCHAR2);

END group_pages_api;
/