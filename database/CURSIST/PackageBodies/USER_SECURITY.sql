CREATE OR REPLACE PACKAGE BODY cursist.user_security IS
  /*
  * Password security API
  */

  /**
  * Returns the Hash for the Username/Password supplied.
  *
  * @param in_username Username
  * @param in_password Password
  *
  * @return Hash for the Username/Password supplied. 
  */
  FUNCTION get_hash(in_username IN VARCHAR2,
                    in_password IN VARCHAR2) RETURN VARCHAR2 IS
    l_salt    VARCHAR2(30) := 'PutYourSaltHere';
  BEGIN
    RETURN dbms_crypto.hash(utl_raw.cast_to_raw(upper(in_username) || l_salt || upper(in_password)), dbms_crypto. hash_sh1);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE;
  END get_hash;
END user_security;
/