CREATE OR REPLACE PACKAGE BODY cursist.front_office IS
  -- Cursor Bodies 
  -- Sub-Program Units 
  FUNCTION check_user(p_username IN VARCHAR2,
                      p_password IN VARCHAR2) RETURN BOOLEAN IS
    -- Check user credentials 
  
    l_dummy NUMBER;
    l_hash  users.password%TYPE;
  
  BEGIN
    l_hash := user_security.get_hash(in_username => p_username, in_password => p_password);
    --
    SELECT 1
    INTO   l_dummy
    FROM   users u
    WHERE  upper(user_name) = upper(p_username)
    AND    password = l_hash
    AND    u.indication_active = 'Y';
  
    RETURN TRUE;
  
  EXCEPTION
    WHEN OTHERS THEN
      -- no_data_found or too_many_rows 
      RETURN FALSE;
    
  END;
  FUNCTION get_use_id(p_username IN VARCHAR2) RETURN NUMBER IS
    -- get user id 
  
    l_use_id NUMBER;
  
  BEGIN
  
    SELECT id
    INTO   l_use_id
    FROM   users
    WHERE  upper(user_name) = upper(p_username);
  
    RETURN l_use_id;
  
  EXCEPTION
    WHEN OTHERS THEN
      -- no_data_found or too_many_rows 
      RETURN 0;
    
  END;
  FUNCTION page_valid(p_apex_id IN NUMBER,
                      p_usr_id  IN NUMBER) RETURN BOOLEAN IS
    -- check if user has access to page 
  
    l_dummy NUMBER;
  
  BEGIN
  
    SELECT 1
    INTO   l_dummy
    FROM   users       usr,
           group_pages gpa,
           pages       pag
    WHERE  usr.id = p_usr_id
    AND    usr.group_id = gpa.group_id
    AND    gpa.page_id = pag.id
    AND    pag.apex_id = p_apex_id;
  
    RETURN TRUE;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN FALSE;
    
  END;

  FUNCTION get_highlight_text(p_index  IN VARCHAR2,
                              p_id     IN VARCHAR2,
                              p_query  IN VARCHAR2,
                              p_amount IN NUMBER := 100) RETURN VARCHAR2 IS
    l_hitab         ctx_doc.highlight_tab;
    l_doc           CLOB;
    l_start         PLS_INTEGER;
    l_start_amount  PLS_INTEGER;
    l_return_string VARCHAR2(30000);
  BEGIN
    ctx_doc.highlight(p_index, p_id, p_query, l_hitab, TRUE);
    ctx_doc.filter(p_index, p_id, l_doc, TRUE);
    IF l_hitab(1).offset > p_amount + 1
    THEN
      l_start        := l_hitab(1).offset - (p_amount + 1);
      l_start_amount := p_amount + 1;
    ELSE
      l_start        := 1;
      l_start_amount := l_hitab(1).offset - 1;
    END IF;
    l_return_string := '...' || dbms_lob.substr(l_doc, l_start_amount, l_start) ||
                       '<FONT style="BACKGROUND-COLOR: yellow; color:black" >' ||
                       dbms_lob.substr(l_doc, l_hitab(1).length, l_hitab(1).offset) || '</FONT>' ||
                       dbms_lob.substr(l_doc, p_amount, l_hitab(1).length + l_hitab(1).offset) || '...';
    RETURN l_return_string;
  END;
  PROCEDURE add_searchterm(in_searchterm IN VARCHAR2,
                           in_use_id     IN NUMBER) IS
  BEGIN
    INSERT INTO search_terms
      (id,
       date_searched,
       term,
       use_id)
    VALUES
      (seq_all.nextval,
       SYSDATE,
       in_searchterm,
       in_use_id);
  END;

  PROCEDURE post_authentication IS
  BEGIN
    apex_util.set_session_state(p_name => 'G_USER_ID', p_value => get_use_id(p_username => v('APP_USER')));
  END;
END front_office;
/