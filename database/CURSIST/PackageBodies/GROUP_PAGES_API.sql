CREATE OR REPLACE PACKAGE BODY cursist.group_pages_api IS
  /*
  * Table API for GROUP_TABLES
  */

  /**
  * Returns a : separated list of all pages for the group given. 
  *
  * @param in_group_id Group t handle.
  *
  * @return The  : separated list of all pages 
  */
  FUNCTION get(in_group_id IN NUMBER) RETURN VARCHAR2 IS
    --
    -- lt_pag    Pages authorized for IN_GROUP_ID 
    lt_pag    apex_t_number;
    l_list    VARCHAR2(4000);
  BEGIN
    IF in_group_id IS NOT NULL
    THEN
      SELECT gpg.page_id
      BULK   COLLECT
      INTO   lt_pag
      FROM   group_pages gpg
      WHERE  gpg.group_id = in_group_id;
      --
      -- Convert the selction to a : separated list.
      l_list := apex_string.join(p_table => lt_pag, p_sep => ':');
    END IF;
    --
    RETURN l_list;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE;
  END get;
  /**
  * Save the selected pages in GROUP_PAGES
  *
  * @param in_group_id Group to handle
  * @param in_pag      : separated list of all authorized tables.
  */
  PROCEDURE put(in_group_id IN NUMBER,
                in_pag      IN VARCHAR2) IS
    --
    -- lt_pag    Pages authorized for IN_GROUP_ID 
    lt_pag    apex_t_number;
  BEGIN
    IF in_group_id IS NOT NULL
    THEN
      DELETE FROM group_pages gpg
      WHERE  gpg.group_id = in_group_id;
      --
      lt_pag := apex_string.split_numbers(p_str => in_pag, p_sep => ':');
      FORALL idx IN 1 .. lt_pag.count
        INSERT INTO group_pages
          (group_id,
           page_id)
        VALUES
          (in_group_id,
           lt_pag(idx));
    
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE;
  END put;
END group_pages_api;
/