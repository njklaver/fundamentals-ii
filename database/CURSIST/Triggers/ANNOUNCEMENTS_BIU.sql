CREATE OR REPLACE trigger cursist.announcements_biu 
    before insert or update  
    on cursist.announcements 
    for each row 
begin 
    if :new.id is null then 
        :new.id := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    end if; 
end announcements_biu;
/