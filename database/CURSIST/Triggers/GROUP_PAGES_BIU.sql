CREATE OR REPLACE trigger cursist.group_pages_biu 
    before insert or update  
    on cursist.group_pages 
    for each row 
begin 
    if :new.id is null then 
        :new.id := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    end if; 
end group_pages_biu;
/