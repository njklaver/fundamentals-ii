CREATE OR REPLACE trigger cursist.documents_biu
    before insert or update
    on cursist.documents
    for each row
begin
    if :new.id is null then
        :new.id := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
        :new.date_created := SYSDATE;
    end if;
end documents_biu;
/