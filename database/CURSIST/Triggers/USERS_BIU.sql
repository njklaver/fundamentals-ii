CREATE OR REPLACE trigger cursist.users_biu 
    before insert or update  
    on cursist.users 
    for each row 
begin 
    if :new.id is null then 
        :new.id := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    end if; 
end users_biu;
/