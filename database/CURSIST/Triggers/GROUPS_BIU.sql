CREATE OR REPLACE trigger cursist.groups_biu 
    before insert or update  
    on cursist.groups 
    for each row 
begin 
    if :new.id is null then 
        :new.id := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    end if; 
end groups_biu;
/