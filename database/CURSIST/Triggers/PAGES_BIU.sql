CREATE OR REPLACE trigger cursist.pages_biu 
    before insert or update  
    on cursist.pages 
    for each row 
begin 
    if :new.id is null then 
        :new.id := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    end if; 
end pages_biu;
/