/**
 * @module util
 * */
const util = {};

const init = ($, debug, item) => {
	/**
	 * Constructs an item name for the current page.
	 *
	 * @param {any} colName Report column name.
	 * @param {any} alias	alias to be used (optional).
	 *
	 * @returns {any} The constructed item name
	 */
	util.getItemName = (colName, alias) => {
		let itemName;
		if (alias) {
			itemName = 'P' + $v('pFlowStepId') + '_' + alias.toUpperCase() + '_' + colName.toUpperCase();
		} else {
			itemName = 'P' + $v('pFlowStepId') + '_' + colName.toUpperCase();
		}

		debug.log(itemName);

		return itemName;
	};

	/**
	 * Copies colValue to the item identified by colName and alias.
	 * @param {any} colName  Report column name.
	 * @param {any} colValue Value to set.
	 * @param {any} alias    alias to be used (optional).
	 */
	util.copyCol = (colName, colValue, alias = '') => {
		const itemName = util.getItemName(colName, alias);
		item(itemName).setValue(colValue);
	};

	/**
	 * Highlights the current record.
	 *
	 * @param {any} alias alias to be used (optional).
	 */
	util.currentRecord = (alias = '')  => {
		const itemName = util.getItemName('ID', alias);
		$('.highLight').removeClass('highLight highLight:hover');
		const itemValue = $v(itemName);
		if (itemValue) {
			$('span[value=' + itemValue + ']').parents('tr').children('td').addClass('highLight');
		}
	};

	return util;
};

export default {
	init
};
