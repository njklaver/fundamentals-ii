/**
 * Home page back-office app
 *
 * @module p1
 */
const p1 = {};

const init =  ($, lang, message, region, debug) =>  {
	/**
    * @member usersIG Static ID Users IG.
    */
	const usersIG = 'usersIG';
	const usersIG$ = apex.jQuery('#' + usersIG);

	/**
     * Users IG initalization code.
     *
     * @function initUsersIG
     *
     *  @param {object} config IG configuration.
     *
     * @returns {object} IG configuration object.
     */
	p1.initUsersIG = function (config) {
		var toolbarData = $.apex.interactiveGrid.copyDefaultToolbar();
		var toolbarGroup = toolbarData.toolbarFind('actions3');

		// Buttons
		toolbarGroup.controls.unshift({
			type: 'BUTTON',
			label: lang.getMessage('FUND2_USER_ACTIVATE_LABEL'),
			action: 'user_activate'
		});
		config.toolbarData = toolbarData;

		// Actions
		config.initActions = function (actions) {
			actions.add([
				{
					name: 'user_activate',
					labelKey: 'FUND2_USER_ACTIVATE_ACTION',
					shortcut: 'Alt+A',
					action: () => {
						apex.event.trigger(usersIG$, 'user_activate$event');
					}
				}
			]);

			// Only enabled when there are records selected
			apex.jQuery(actions.context).on('interactivegridselectionchange', (event, data) => {
				var method = 'enable';
				if (data.selectedRecords.length === 0) {
					method = 'disable';
				}

				actions[method]('user_activate');
			});
		};

		return config;
	};

	/**
	 * Activates the user.
	 *
	 * @function activate
	 */
	p1.activate = function () {
		var view = region(usersIG).call('getViews', 'grid');
		var {model} = view;
		var selected = view.getSelectedRecords();

		selected.forEach((_object, index) => {
			var rec = selected[index];
			model.setValue(rec, 'INDICATION_ACTIVE', 'Y');
		});
	};

	
	$( () => {
		/**
		 * @event interactivegridviewchange
		 * */
		usersIG$.on('interactivegridcreate', () => {
			var view = region(usersIG).call('getViews', 'grid');
			var items;
			var menu$ = view.selActionMenu$;

			items = menu$.menu('option').items;
			items.push({
				type: 'separator'
			});
			items.push({
				id: 'user_activate',
				type: 'action',
				action: 'user_activate'
			});
		});
	});
  
  return p1;
};

export default {
	init
};