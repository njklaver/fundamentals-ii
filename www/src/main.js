import p1 from "./js/p1";
import util from "./js/util";

var app;
var ig;

apex.jQuery(document).ready(() => {
	const pageId = Number(document.getElementById("pFlowStepId").value);
  if (pageId === 1) {
    ig = p1.init(apex.jQuery, apex.lang, apex.message, apex.region, apex.debug);
  }
  if (pageId === 15) {
    app = util.init(apex.jQuery, apex.debug, apex.item);
  }
});

export {
  app,
  ig
}
