/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is not neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
var fund2;fund2 =
/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/p1.js":
/*!**********************!*\
  !*** ./src/js/p1.js ***!
  \**********************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.* */
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/**\n * Home page back-office app\n *\n * @module p1\n */\nvar p1 = {};\n\nvar init = function init($, lang, message, region, debug) {\n  /**\n     * @member usersIG Static ID Users IG.\n     */\n  var usersIG = 'usersIG';\n  var usersIG$ = apex.jQuery('#' + usersIG);\n  /**\n      * Users IG initalization code.\n      *\n      * @function initUsersIG\n      *\n      *  @param {object} config IG configuration.\n      *\n      * @returns {object} IG configuration object.\n      */\n\n  p1.initUsersIG = function (config) {\n    var toolbarData = $.apex.interactiveGrid.copyDefaultToolbar();\n    var toolbarGroup = toolbarData.toolbarFind('actions3'); // Buttons\n\n    toolbarGroup.controls.unshift({\n      type: 'BUTTON',\n      label: lang.getMessage('FUND2_USER_ACTIVATE_LABEL'),\n      action: 'user_activate'\n    });\n    config.toolbarData = toolbarData; // Actions\n\n    config.initActions = function (actions) {\n      actions.add([{\n        name: 'user_activate',\n        labelKey: 'FUND2_USER_ACTIVATE_ACTION',\n        shortcut: 'Alt+A',\n        action: function action() {\n          apex.event.trigger(usersIG$, 'user_activate$event');\n        }\n      }]); // Only enabled when there are records selected\n\n      apex.jQuery(actions.context).on('interactivegridselectionchange', function (event, data) {\n        var method = 'enable';\n\n        if (data.selectedRecords.length === 0) {\n          method = 'disable';\n        }\n\n        actions[method]('user_activate');\n      });\n    };\n\n    return config;\n  };\n  /**\n   * Activates the user.\n   *\n   * @function activate\n   */\n\n\n  p1.activate = function () {\n    var view = region(usersIG).call('getViews', 'grid');\n    var model = view.model;\n    var selected = view.getSelectedRecords();\n    selected.forEach(function (_object, index) {\n      var rec = selected[index];\n      model.setValue(rec, 'INDICATION_ACTIVE', 'Y');\n    });\n  };\n\n  $(function () {\n    /**\n     * @event interactivegridviewchange\n     * */\n    usersIG$.on('interactivegridcreate', function () {\n      var view = region(usersIG).call('getViews', 'grid');\n      var items;\n      var menu$ = view.selActionMenu$;\n      items = menu$.menu('option').items;\n      items.push({\n        type: 'separator'\n      });\n      items.push({\n        id: 'user_activate',\n        type: 'action',\n        action: 'user_activate'\n      });\n    });\n  });\n  return p1;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  init: init\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mdW5kMi8uL3NyYy9qcy9wMS5qcz9jMjExIl0sIm5hbWVzIjpbInAxIiwiaW5pdCIsIiQiLCJsYW5nIiwibWVzc2FnZSIsInJlZ2lvbiIsImRlYnVnIiwidXNlcnNJRyIsInVzZXJzSUckIiwiYXBleCIsImpRdWVyeSIsImluaXRVc2Vyc0lHIiwiY29uZmlnIiwidG9vbGJhckRhdGEiLCJpbnRlcmFjdGl2ZUdyaWQiLCJjb3B5RGVmYXVsdFRvb2xiYXIiLCJ0b29sYmFyR3JvdXAiLCJ0b29sYmFyRmluZCIsImNvbnRyb2xzIiwidW5zaGlmdCIsInR5cGUiLCJsYWJlbCIsImdldE1lc3NhZ2UiLCJhY3Rpb24iLCJpbml0QWN0aW9ucyIsImFjdGlvbnMiLCJhZGQiLCJuYW1lIiwibGFiZWxLZXkiLCJzaG9ydGN1dCIsImV2ZW50IiwidHJpZ2dlciIsImNvbnRleHQiLCJvbiIsImRhdGEiLCJtZXRob2QiLCJzZWxlY3RlZFJlY29yZHMiLCJsZW5ndGgiLCJhY3RpdmF0ZSIsInZpZXciLCJjYWxsIiwibW9kZWwiLCJzZWxlY3RlZCIsImdldFNlbGVjdGVkUmVjb3JkcyIsImZvckVhY2giLCJfb2JqZWN0IiwiaW5kZXgiLCJyZWMiLCJzZXRWYWx1ZSIsIml0ZW1zIiwibWVudSQiLCJzZWxBY3Rpb25NZW51JCIsIm1lbnUiLCJwdXNoIiwiaWQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBTUEsRUFBRSxHQUFHLEVBQVg7O0FBRUEsSUFBTUMsSUFBSSxHQUFJLFNBQVJBLElBQVEsQ0FBQ0MsQ0FBRCxFQUFJQyxJQUFKLEVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCQyxLQUEzQixFQUFzQztBQUNuRDtBQUNEO0FBQ0E7QUFDQyxNQUFNQyxPQUFPLEdBQUcsU0FBaEI7QUFDQSxNQUFNQyxRQUFRLEdBQUdDLElBQUksQ0FBQ0MsTUFBTCxDQUFZLE1BQU1ILE9BQWxCLENBQWpCO0FBRUE7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNDUCxJQUFFLENBQUNXLFdBQUgsR0FBaUIsVUFBVUMsTUFBVixFQUFrQjtBQUNsQyxRQUFJQyxXQUFXLEdBQUdYLENBQUMsQ0FBQ08sSUFBRixDQUFPSyxlQUFQLENBQXVCQyxrQkFBdkIsRUFBbEI7QUFDQSxRQUFJQyxZQUFZLEdBQUdILFdBQVcsQ0FBQ0ksV0FBWixDQUF3QixVQUF4QixDQUFuQixDQUZrQyxDQUlsQzs7QUFDQUQsZ0JBQVksQ0FBQ0UsUUFBYixDQUFzQkMsT0FBdEIsQ0FBOEI7QUFDN0JDLFVBQUksRUFBRSxRQUR1QjtBQUU3QkMsV0FBSyxFQUFFbEIsSUFBSSxDQUFDbUIsVUFBTCxDQUFnQiwyQkFBaEIsQ0FGc0I7QUFHN0JDLFlBQU0sRUFBRTtBQUhxQixLQUE5QjtBQUtBWCxVQUFNLENBQUNDLFdBQVAsR0FBcUJBLFdBQXJCLENBVmtDLENBWWxDOztBQUNBRCxVQUFNLENBQUNZLFdBQVAsR0FBcUIsVUFBVUMsT0FBVixFQUFtQjtBQUN2Q0EsYUFBTyxDQUFDQyxHQUFSLENBQVksQ0FDWDtBQUNDQyxZQUFJLEVBQUUsZUFEUDtBQUVDQyxnQkFBUSxFQUFFLDRCQUZYO0FBR0NDLGdCQUFRLEVBQUUsT0FIWDtBQUlDTixjQUFNLEVBQUUsa0JBQU07QUFDYmQsY0FBSSxDQUFDcUIsS0FBTCxDQUFXQyxPQUFYLENBQW1CdkIsUUFBbkIsRUFBNkIscUJBQTdCO0FBQ0E7QUFORixPQURXLENBQVosRUFEdUMsQ0FZdkM7O0FBQ0FDLFVBQUksQ0FBQ0MsTUFBTCxDQUFZZSxPQUFPLENBQUNPLE9BQXBCLEVBQTZCQyxFQUE3QixDQUFnQyxnQ0FBaEMsRUFBa0UsVUFBQ0gsS0FBRCxFQUFRSSxJQUFSLEVBQWlCO0FBQ2xGLFlBQUlDLE1BQU0sR0FBRyxRQUFiOztBQUNBLFlBQUlELElBQUksQ0FBQ0UsZUFBTCxDQUFxQkMsTUFBckIsS0FBZ0MsQ0FBcEMsRUFBdUM7QUFDdENGLGdCQUFNLEdBQUcsU0FBVDtBQUNBOztBQUVEVixlQUFPLENBQUNVLE1BQUQsQ0FBUCxDQUFnQixlQUFoQjtBQUNBLE9BUEQ7QUFRQSxLQXJCRDs7QUF1QkEsV0FBT3ZCLE1BQVA7QUFDQSxHQXJDRDtBQXVDQTtBQUNEO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQ1osSUFBRSxDQUFDc0MsUUFBSCxHQUFjLFlBQVk7QUFDekIsUUFBSUMsSUFBSSxHQUFHbEMsTUFBTSxDQUFDRSxPQUFELENBQU4sQ0FBZ0JpQyxJQUFoQixDQUFxQixVQUFyQixFQUFpQyxNQUFqQyxDQUFYO0FBRHlCLFFBRXBCQyxLQUZvQixHQUVYRixJQUZXLENBRXBCRSxLQUZvQjtBQUd6QixRQUFJQyxRQUFRLEdBQUdILElBQUksQ0FBQ0ksa0JBQUwsRUFBZjtBQUVBRCxZQUFRLENBQUNFLE9BQVQsQ0FBaUIsVUFBQ0MsT0FBRCxFQUFVQyxLQUFWLEVBQW9CO0FBQ3BDLFVBQUlDLEdBQUcsR0FBR0wsUUFBUSxDQUFDSSxLQUFELENBQWxCO0FBQ0FMLFdBQUssQ0FBQ08sUUFBTixDQUFlRCxHQUFmLEVBQW9CLG1CQUFwQixFQUF5QyxHQUF6QztBQUNBLEtBSEQ7QUFJQSxHQVREOztBQVlBN0MsR0FBQyxDQUFFLFlBQU07QUFDUjtBQUNGO0FBQ0E7QUFDRU0sWUFBUSxDQUFDeUIsRUFBVCxDQUFZLHVCQUFaLEVBQXFDLFlBQU07QUFDMUMsVUFBSU0sSUFBSSxHQUFHbEMsTUFBTSxDQUFDRSxPQUFELENBQU4sQ0FBZ0JpQyxJQUFoQixDQUFxQixVQUFyQixFQUFpQyxNQUFqQyxDQUFYO0FBQ0EsVUFBSVMsS0FBSjtBQUNBLFVBQUlDLEtBQUssR0FBR1gsSUFBSSxDQUFDWSxjQUFqQjtBQUVBRixXQUFLLEdBQUdDLEtBQUssQ0FBQ0UsSUFBTixDQUFXLFFBQVgsRUFBcUJILEtBQTdCO0FBQ0FBLFdBQUssQ0FBQ0ksSUFBTixDQUFXO0FBQ1ZqQyxZQUFJLEVBQUU7QUFESSxPQUFYO0FBR0E2QixXQUFLLENBQUNJLElBQU4sQ0FBVztBQUNWQyxVQUFFLEVBQUUsZUFETTtBQUVWbEMsWUFBSSxFQUFFLFFBRkk7QUFHVkcsY0FBTSxFQUFFO0FBSEUsT0FBWDtBQUtBLEtBZEQ7QUFlQSxHQW5CQSxDQUFEO0FBcUJDLFNBQU92QixFQUFQO0FBQ0QsQ0E5RkQ7O0FBZ0dBLCtEQUFlO0FBQ2RDLE1BQUksRUFBSkE7QUFEYyxDQUFmIiwiZmlsZSI6Ii4vc3JjL2pzL3AxLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBIb21lIHBhZ2UgYmFjay1vZmZpY2UgYXBwXG4gKlxuICogQG1vZHVsZSBwMVxuICovXG5jb25zdCBwMSA9IHt9O1xuXG5jb25zdCBpbml0ID0gICgkLCBsYW5nLCBtZXNzYWdlLCByZWdpb24sIGRlYnVnKSA9PiAge1xuXHQvKipcbiAgICAqIEBtZW1iZXIgdXNlcnNJRyBTdGF0aWMgSUQgVXNlcnMgSUcuXG4gICAgKi9cblx0Y29uc3QgdXNlcnNJRyA9ICd1c2Vyc0lHJztcblx0Y29uc3QgdXNlcnNJRyQgPSBhcGV4LmpRdWVyeSgnIycgKyB1c2Vyc0lHKTtcblxuXHQvKipcbiAgICAgKiBVc2VycyBJRyBpbml0YWxpemF0aW9uIGNvZGUuXG4gICAgICpcbiAgICAgKiBAZnVuY3Rpb24gaW5pdFVzZXJzSUdcbiAgICAgKlxuICAgICAqICBAcGFyYW0ge29iamVjdH0gY29uZmlnIElHIGNvbmZpZ3VyYXRpb24uXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7b2JqZWN0fSBJRyBjb25maWd1cmF0aW9uIG9iamVjdC5cbiAgICAgKi9cblx0cDEuaW5pdFVzZXJzSUcgPSBmdW5jdGlvbiAoY29uZmlnKSB7XG5cdFx0dmFyIHRvb2xiYXJEYXRhID0gJC5hcGV4LmludGVyYWN0aXZlR3JpZC5jb3B5RGVmYXVsdFRvb2xiYXIoKTtcblx0XHR2YXIgdG9vbGJhckdyb3VwID0gdG9vbGJhckRhdGEudG9vbGJhckZpbmQoJ2FjdGlvbnMzJyk7XG5cblx0XHQvLyBCdXR0b25zXG5cdFx0dG9vbGJhckdyb3VwLmNvbnRyb2xzLnVuc2hpZnQoe1xuXHRcdFx0dHlwZTogJ0JVVFRPTicsXG5cdFx0XHRsYWJlbDogbGFuZy5nZXRNZXNzYWdlKCdGVU5EMl9VU0VSX0FDVElWQVRFX0xBQkVMJyksXG5cdFx0XHRhY3Rpb246ICd1c2VyX2FjdGl2YXRlJ1xuXHRcdH0pO1xuXHRcdGNvbmZpZy50b29sYmFyRGF0YSA9IHRvb2xiYXJEYXRhO1xuXG5cdFx0Ly8gQWN0aW9uc1xuXHRcdGNvbmZpZy5pbml0QWN0aW9ucyA9IGZ1bmN0aW9uIChhY3Rpb25zKSB7XG5cdFx0XHRhY3Rpb25zLmFkZChbXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRuYW1lOiAndXNlcl9hY3RpdmF0ZScsXG5cdFx0XHRcdFx0bGFiZWxLZXk6ICdGVU5EMl9VU0VSX0FDVElWQVRFX0FDVElPTicsXG5cdFx0XHRcdFx0c2hvcnRjdXQ6ICdBbHQrQScsXG5cdFx0XHRcdFx0YWN0aW9uOiAoKSA9PiB7XG5cdFx0XHRcdFx0XHRhcGV4LmV2ZW50LnRyaWdnZXIodXNlcnNJRyQsICd1c2VyX2FjdGl2YXRlJGV2ZW50Jyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRdKTtcblxuXHRcdFx0Ly8gT25seSBlbmFibGVkIHdoZW4gdGhlcmUgYXJlIHJlY29yZHMgc2VsZWN0ZWRcblx0XHRcdGFwZXgualF1ZXJ5KGFjdGlvbnMuY29udGV4dCkub24oJ2ludGVyYWN0aXZlZ3JpZHNlbGVjdGlvbmNoYW5nZScsIChldmVudCwgZGF0YSkgPT4ge1xuXHRcdFx0XHR2YXIgbWV0aG9kID0gJ2VuYWJsZSc7XG5cdFx0XHRcdGlmIChkYXRhLnNlbGVjdGVkUmVjb3Jkcy5sZW5ndGggPT09IDApIHtcblx0XHRcdFx0XHRtZXRob2QgPSAnZGlzYWJsZSc7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRhY3Rpb25zW21ldGhvZF0oJ3VzZXJfYWN0aXZhdGUnKTtcblx0XHRcdH0pO1xuXHRcdH07XG5cblx0XHRyZXR1cm4gY29uZmlnO1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBBY3RpdmF0ZXMgdGhlIHVzZXIuXG5cdCAqXG5cdCAqIEBmdW5jdGlvbiBhY3RpdmF0ZVxuXHQgKi9cblx0cDEuYWN0aXZhdGUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0dmFyIHZpZXcgPSByZWdpb24odXNlcnNJRykuY2FsbCgnZ2V0Vmlld3MnLCAnZ3JpZCcpO1xuXHRcdHZhciB7bW9kZWx9ID0gdmlldztcblx0XHR2YXIgc2VsZWN0ZWQgPSB2aWV3LmdldFNlbGVjdGVkUmVjb3JkcygpO1xuXG5cdFx0c2VsZWN0ZWQuZm9yRWFjaCgoX29iamVjdCwgaW5kZXgpID0+IHtcblx0XHRcdHZhciByZWMgPSBzZWxlY3RlZFtpbmRleF07XG5cdFx0XHRtb2RlbC5zZXRWYWx1ZShyZWMsICdJTkRJQ0FUSU9OX0FDVElWRScsICdZJyk7XG5cdFx0fSk7XG5cdH07XG5cblx0XG5cdCQoICgpID0+IHtcblx0XHQvKipcblx0XHQgKiBAZXZlbnQgaW50ZXJhY3RpdmVncmlkdmlld2NoYW5nZVxuXHRcdCAqICovXG5cdFx0dXNlcnNJRyQub24oJ2ludGVyYWN0aXZlZ3JpZGNyZWF0ZScsICgpID0+IHtcblx0XHRcdHZhciB2aWV3ID0gcmVnaW9uKHVzZXJzSUcpLmNhbGwoJ2dldFZpZXdzJywgJ2dyaWQnKTtcblx0XHRcdHZhciBpdGVtcztcblx0XHRcdHZhciBtZW51JCA9IHZpZXcuc2VsQWN0aW9uTWVudSQ7XG5cblx0XHRcdGl0ZW1zID0gbWVudSQubWVudSgnb3B0aW9uJykuaXRlbXM7XG5cdFx0XHRpdGVtcy5wdXNoKHtcblx0XHRcdFx0dHlwZTogJ3NlcGFyYXRvcidcblx0XHRcdH0pO1xuXHRcdFx0aXRlbXMucHVzaCh7XG5cdFx0XHRcdGlkOiAndXNlcl9hY3RpdmF0ZScsXG5cdFx0XHRcdHR5cGU6ICdhY3Rpb24nLFxuXHRcdFx0XHRhY3Rpb246ICd1c2VyX2FjdGl2YXRlJ1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdH0pO1xuICBcbiAgcmV0dXJuIHAxO1xufTtcblxuZXhwb3J0IGRlZmF1bHQge1xuXHRpbml0XG59OyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/js/p1.js\n");

/***/ }),

/***/ "./src/js/util.js":
/*!************************!*\
  !*** ./src/js/util.js ***!
  \************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.* */
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/**\r\n * @module util\r\n * */\nvar util = {};\n\nvar init = function init($, debug, item) {\n  /**\r\n   * Constructs an item name for the current page.\r\n   *\r\n   * @param {any} colName Report column name.\r\n   * @param {any} alias\talias to be used (optional).\r\n   *\r\n   * @returns {any} The constructed item name\r\n   */\n  util.getItemName = function (colName, alias) {\n    var itemName;\n\n    if (alias) {\n      itemName = 'P' + $v('pFlowStepId') + '_' + alias.toUpperCase() + '_' + colName.toUpperCase();\n    } else {\n      itemName = 'P' + $v('pFlowStepId') + '_' + colName.toUpperCase();\n    }\n\n    debug.log(itemName);\n    return itemName;\n  };\n  /**\r\n   * Copies colValue to the item identified by colName and alias.\r\n   * @param {any} colName  Report column name.\r\n   * @param {any} colValue Value to set.\r\n   * @param {any} alias    alias to be used (optional).\r\n   */\n\n\n  util.copyCol = function (colName, colValue) {\n    var alias = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';\n    var itemName = util.getItemName(colName, alias);\n    item(itemName).setValue(colValue);\n  };\n  /**\r\n   * Highlights the current record.\r\n   *\r\n   * @param {any} alias alias to be used (optional).\r\n   */\n\n\n  util.currentRecord = function () {\n    var alias = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';\n    var itemName = util.getItemName('ID', alias);\n    $('.highLight').removeClass('highLight highLight:hover');\n    var itemValue = $v(itemName);\n\n    if (itemValue) {\n      $('span[value=' + itemValue + ']').parents('tr').children('td').addClass('highLight');\n    }\n  };\n\n  return util;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  init: init\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mdW5kMi8uL3NyYy9qcy91dGlsLmpzP2Y2NDQiXSwibmFtZXMiOlsidXRpbCIsImluaXQiLCIkIiwiZGVidWciLCJpdGVtIiwiZ2V0SXRlbU5hbWUiLCJjb2xOYW1lIiwiYWxpYXMiLCJpdGVtTmFtZSIsIiR2IiwidG9VcHBlckNhc2UiLCJsb2ciLCJjb3B5Q29sIiwiY29sVmFsdWUiLCJzZXRWYWx1ZSIsImN1cnJlbnRSZWNvcmQiLCJyZW1vdmVDbGFzcyIsIml0ZW1WYWx1ZSIsInBhcmVudHMiLCJjaGlsZHJlbiIsImFkZENsYXNzIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0EsSUFBTUEsSUFBSSxHQUFHLEVBQWI7O0FBRUEsSUFBTUMsSUFBSSxHQUFHLFNBQVBBLElBQU8sQ0FBQ0MsQ0FBRCxFQUFJQyxLQUFKLEVBQVdDLElBQVgsRUFBb0I7QUFDaEM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNDSixNQUFJLENBQUNLLFdBQUwsR0FBbUIsVUFBQ0MsT0FBRCxFQUFVQyxLQUFWLEVBQW9CO0FBQ3RDLFFBQUlDLFFBQUo7O0FBQ0EsUUFBSUQsS0FBSixFQUFXO0FBQ1ZDLGNBQVEsR0FBRyxNQUFNQyxFQUFFLENBQUMsYUFBRCxDQUFSLEdBQTBCLEdBQTFCLEdBQWdDRixLQUFLLENBQUNHLFdBQU4sRUFBaEMsR0FBc0QsR0FBdEQsR0FBNERKLE9BQU8sQ0FBQ0ksV0FBUixFQUF2RTtBQUNBLEtBRkQsTUFFTztBQUNORixjQUFRLEdBQUcsTUFBTUMsRUFBRSxDQUFDLGFBQUQsQ0FBUixHQUEwQixHQUExQixHQUFnQ0gsT0FBTyxDQUFDSSxXQUFSLEVBQTNDO0FBQ0E7O0FBRURQLFNBQUssQ0FBQ1EsR0FBTixDQUFVSCxRQUFWO0FBRUEsV0FBT0EsUUFBUDtBQUNBLEdBWEQ7QUFhQTtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNDUixNQUFJLENBQUNZLE9BQUwsR0FBZSxVQUFDTixPQUFELEVBQVVPLFFBQVYsRUFBbUM7QUFBQSxRQUFmTixLQUFlLHVFQUFQLEVBQU87QUFDakQsUUFBTUMsUUFBUSxHQUFHUixJQUFJLENBQUNLLFdBQUwsQ0FBaUJDLE9BQWpCLEVBQTBCQyxLQUExQixDQUFqQjtBQUNBSCxRQUFJLENBQUNJLFFBQUQsQ0FBSixDQUFlTSxRQUFmLENBQXdCRCxRQUF4QjtBQUNBLEdBSEQ7QUFLQTtBQUNEO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQ2IsTUFBSSxDQUFDZSxhQUFMLEdBQXFCLFlBQWlCO0FBQUEsUUFBaEJSLEtBQWdCLHVFQUFSLEVBQVE7QUFDckMsUUFBTUMsUUFBUSxHQUFHUixJQUFJLENBQUNLLFdBQUwsQ0FBaUIsSUFBakIsRUFBdUJFLEtBQXZCLENBQWpCO0FBQ0FMLEtBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JjLFdBQWhCLENBQTRCLDJCQUE1QjtBQUNBLFFBQU1DLFNBQVMsR0FBR1IsRUFBRSxDQUFDRCxRQUFELENBQXBCOztBQUNBLFFBQUlTLFNBQUosRUFBZTtBQUNkZixPQUFDLENBQUMsZ0JBQWdCZSxTQUFoQixHQUE0QixHQUE3QixDQUFELENBQW1DQyxPQUFuQyxDQUEyQyxJQUEzQyxFQUFpREMsUUFBakQsQ0FBMEQsSUFBMUQsRUFBZ0VDLFFBQWhFLENBQXlFLFdBQXpFO0FBQ0E7QUFDRCxHQVBEOztBQVNBLFNBQU9wQixJQUFQO0FBQ0EsQ0FoREQ7O0FBa0RBLCtEQUFlO0FBQ2RDLE1BQUksRUFBSkE7QUFEYyxDQUFmIiwiZmlsZSI6Ii4vc3JjL2pzL3V0aWwuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQG1vZHVsZSB1dGlsXHJcbiAqICovXHJcbmNvbnN0IHV0aWwgPSB7fTtcclxuXHJcbmNvbnN0IGluaXQgPSAoJCwgZGVidWcsIGl0ZW0pID0+IHtcclxuXHQvKipcclxuXHQgKiBDb25zdHJ1Y3RzIGFuIGl0ZW0gbmFtZSBmb3IgdGhlIGN1cnJlbnQgcGFnZS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7YW55fSBjb2xOYW1lIFJlcG9ydCBjb2x1bW4gbmFtZS5cclxuXHQgKiBAcGFyYW0ge2FueX0gYWxpYXNcdGFsaWFzIHRvIGJlIHVzZWQgKG9wdGlvbmFsKS5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm5zIHthbnl9IFRoZSBjb25zdHJ1Y3RlZCBpdGVtIG5hbWVcclxuXHQgKi9cclxuXHR1dGlsLmdldEl0ZW1OYW1lID0gKGNvbE5hbWUsIGFsaWFzKSA9PiB7XHJcblx0XHRsZXQgaXRlbU5hbWU7XHJcblx0XHRpZiAoYWxpYXMpIHtcclxuXHRcdFx0aXRlbU5hbWUgPSAnUCcgKyAkdigncEZsb3dTdGVwSWQnKSArICdfJyArIGFsaWFzLnRvVXBwZXJDYXNlKCkgKyAnXycgKyBjb2xOYW1lLnRvVXBwZXJDYXNlKCk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRpdGVtTmFtZSA9ICdQJyArICR2KCdwRmxvd1N0ZXBJZCcpICsgJ18nICsgY29sTmFtZS50b1VwcGVyQ2FzZSgpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnLmxvZyhpdGVtTmFtZSk7XHJcblxyXG5cdFx0cmV0dXJuIGl0ZW1OYW1lO1xyXG5cdH07XHJcblxyXG5cdC8qKlxyXG5cdCAqIENvcGllcyBjb2xWYWx1ZSB0byB0aGUgaXRlbSBpZGVudGlmaWVkIGJ5IGNvbE5hbWUgYW5kIGFsaWFzLlxyXG5cdCAqIEBwYXJhbSB7YW55fSBjb2xOYW1lICBSZXBvcnQgY29sdW1uIG5hbWUuXHJcblx0ICogQHBhcmFtIHthbnl9IGNvbFZhbHVlIFZhbHVlIHRvIHNldC5cclxuXHQgKiBAcGFyYW0ge2FueX0gYWxpYXMgICAgYWxpYXMgdG8gYmUgdXNlZCAob3B0aW9uYWwpLlxyXG5cdCAqL1xyXG5cdHV0aWwuY29weUNvbCA9IChjb2xOYW1lLCBjb2xWYWx1ZSwgYWxpYXMgPSAnJykgPT4ge1xyXG5cdFx0Y29uc3QgaXRlbU5hbWUgPSB1dGlsLmdldEl0ZW1OYW1lKGNvbE5hbWUsIGFsaWFzKTtcclxuXHRcdGl0ZW0oaXRlbU5hbWUpLnNldFZhbHVlKGNvbFZhbHVlKTtcclxuXHR9O1xyXG5cclxuXHQvKipcclxuXHQgKiBIaWdobGlnaHRzIHRoZSBjdXJyZW50IHJlY29yZC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7YW55fSBhbGlhcyBhbGlhcyB0byBiZSB1c2VkIChvcHRpb25hbCkuXHJcblx0ICovXHJcblx0dXRpbC5jdXJyZW50UmVjb3JkID0gKGFsaWFzID0gJycpICA9PiB7XHJcblx0XHRjb25zdCBpdGVtTmFtZSA9IHV0aWwuZ2V0SXRlbU5hbWUoJ0lEJywgYWxpYXMpO1xyXG5cdFx0JCgnLmhpZ2hMaWdodCcpLnJlbW92ZUNsYXNzKCdoaWdoTGlnaHQgaGlnaExpZ2h0OmhvdmVyJyk7XHJcblx0XHRjb25zdCBpdGVtVmFsdWUgPSAkdihpdGVtTmFtZSk7XHJcblx0XHRpZiAoaXRlbVZhbHVlKSB7XHJcblx0XHRcdCQoJ3NwYW5bdmFsdWU9JyArIGl0ZW1WYWx1ZSArICddJykucGFyZW50cygndHInKS5jaGlsZHJlbigndGQnKS5hZGRDbGFzcygnaGlnaExpZ2h0Jyk7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0cmV0dXJuIHV0aWw7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0aW5pdFxyXG59O1xyXG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/js/util.js\n");

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! namespace exports */
/*! export app [provided] [maybe used in main (runtime-defined)] [usage prevents renaming] */
/*! export ig [provided] [maybe used in main (runtime-defined)] [usage prevents renaming] */
/*! other exports [not provided] [maybe used in main (runtime-defined)] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.d, __webpack_require__.* */
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"app\": function() { return /* binding */ app; },\n/* harmony export */   \"ig\": function() { return /* binding */ ig; }\n/* harmony export */ });\n/* harmony import */ var _js_p1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/p1 */ \"./src/js/p1.js\");\n/* harmony import */ var _js_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/util */ \"./src/js/util.js\");\n\n\nvar app;\nvar ig;\napex.jQuery(document).ready(function () {\n  var pageId = Number(document.getElementById(\"pFlowStepId\").value);\n\n  if (pageId === 1) {\n    ig = _js_p1__WEBPACK_IMPORTED_MODULE_0__.default.init(apex.jQuery, apex.lang, apex.message, apex.region, apex.debug);\n  }\n\n  if (pageId === 15) {\n    app = _js_util__WEBPACK_IMPORTED_MODULE_1__.default.init(apex.jQuery, apex.debug, apex.item);\n  }\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mdW5kMi8uL3NyYy9tYWluLmpzPzU2ZDciXSwibmFtZXMiOlsiYXBwIiwiaWciLCJhcGV4IiwialF1ZXJ5IiwiZG9jdW1lbnQiLCJyZWFkeSIsInBhZ2VJZCIsIk51bWJlciIsImdldEVsZW1lbnRCeUlkIiwidmFsdWUiLCJwMSIsImxhbmciLCJtZXNzYWdlIiwicmVnaW9uIiwiZGVidWciLCJ1dGlsIiwiaXRlbSJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFFQSxJQUFJQSxHQUFKO0FBQ0EsSUFBSUMsRUFBSjtBQUVBQyxJQUFJLENBQUNDLE1BQUwsQ0FBWUMsUUFBWixFQUFzQkMsS0FBdEIsQ0FBNEIsWUFBTTtBQUNqQyxNQUFNQyxNQUFNLEdBQUdDLE1BQU0sQ0FBQ0gsUUFBUSxDQUFDSSxjQUFULENBQXdCLGFBQXhCLEVBQXVDQyxLQUF4QyxDQUFyQjs7QUFDQyxNQUFJSCxNQUFNLEtBQUssQ0FBZixFQUFrQjtBQUNoQkwsTUFBRSxHQUFHUyxnREFBQSxDQUFRUixJQUFJLENBQUNDLE1BQWIsRUFBcUJELElBQUksQ0FBQ1MsSUFBMUIsRUFBZ0NULElBQUksQ0FBQ1UsT0FBckMsRUFBOENWLElBQUksQ0FBQ1csTUFBbkQsRUFBMkRYLElBQUksQ0FBQ1ksS0FBaEUsQ0FBTDtBQUNEOztBQUNELE1BQUlSLE1BQU0sS0FBSyxFQUFmLEVBQW1CO0FBQ2pCTixPQUFHLEdBQUdlLGtEQUFBLENBQVViLElBQUksQ0FBQ0MsTUFBZixFQUF1QkQsSUFBSSxDQUFDWSxLQUE1QixFQUFtQ1osSUFBSSxDQUFDYyxJQUF4QyxDQUFOO0FBQ0Q7QUFDRixDQVJEIiwiZmlsZSI6Ii4vc3JjL21haW4uanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcDEgZnJvbSBcIi4vanMvcDFcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuL2pzL3V0aWxcIjtcblxudmFyIGFwcDtcbnZhciBpZztcblxuYXBleC5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KCgpID0+IHtcblx0Y29uc3QgcGFnZUlkID0gTnVtYmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicEZsb3dTdGVwSWRcIikudmFsdWUpO1xuICBpZiAocGFnZUlkID09PSAxKSB7XG4gICAgaWcgPSBwMS5pbml0KGFwZXgualF1ZXJ5LCBhcGV4LmxhbmcsIGFwZXgubWVzc2FnZSwgYXBleC5yZWdpb24sIGFwZXguZGVidWcpO1xuICB9XG4gIGlmIChwYWdlSWQgPT09IDE1KSB7XG4gICAgYXBwID0gdXRpbC5pbml0KGFwZXgualF1ZXJ5LCBhcGV4LmRlYnVnLCBhcGV4Lml0ZW0pO1xuICB9XG59KTtcblxuZXhwb3J0IHtcbiAgYXBwLFxuICBpZ1xufVxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/main.js\n");

/***/ }),

/***/ "./src/main.scss":
/*!***********************!*\
  !*** ./src/main.scss ***!
  \***********************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.p, __webpack_require__.* */
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"fund2.css\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mdW5kMi8uL3NyYy9tYWluLnNjc3M/OWM2YSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsK0RBQWUscUJBQXVCLGNBQWMiLCJmaWxlIjoiLi9zcmMvbWFpbi5zY3NzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcImZ1bmQyLmNzc1wiOyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/main.scss\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	// module exports must be returned from runtime so entry inlining is disabled
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	__webpack_require__("./src/main.scss");
/******/ 	return __webpack_require__("./src/main.js");
/******/ })()
;